CREATE  DB hospitalMangementSystem;
USE hospital;

-- Table for Patients
CREATE TABLE Patients (
    PatientID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Gender VARCHAR(10),
    Weight DECIMAL(5,2),
    Height DECIMAL(5,2),
    DrID INT,
    FOREIGN KEY (DrID) REFERENCES Doctors(DoctorID)
);

-- Table for Doctors
CREATE TABLE Doctors (
    DoctorID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Gender VARCHAR(10),
    Specialization VARCHAR(255),
    ExperienceYears INT,
    Salary DECIMAL(10,2),
    DepartmentID INT,
    FOREIGN KEY (DepID) REFERENCES Departments(DepartmentID)
);

-- Table for Nurses
CREATE TABLE Nurses (
    NurseID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Gender VARCHAR(10),
    Salary DECIMAL(10,2),
  --DepID INT,
    DepartmentID INT,
    FOREIGN KEY (DepartmentID) REFERENCES Departments(DepartmentID)
);

-- Table for Rooms
CREATE TABLE Rooms (
    RoomNumber INT PRIMARY KEY,
    RoomType VARCHAR(255),
    NurseID INT,
    Status VARCHAR(255),
    FOREIGN KEY (NurseID) REFERENCES Nurses(NurseID)
);

-- Table for Administrators
CREATE TABLE Administrators (
    AdminID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Gender VARCHAR(10),
    Age INT,
    DepartmentID INT,
    Position VARCHAR(255),
    FOREIGN KEY (DepartmentID) REFERENCES Departments(DepartmentID)
);

-- Table for Departments
CREATE TABLE Departments (
    DepartmentID INT PRIMARY KEY,
    DepartmentName VARCHAR(255),
    Description VARCHAR(255)
);

-- Table for Referrals
CREATE TABLE Referrals (
    ReferralID INT PRIMARY KEY,
    PatientID INT,
    ReferringDoctorID INT,
    ReferralHospital VARCHAR(100),
    ReferredHospital VARCHAR(100),
    ReferralReason TEXT,
    ReferralDate DATE,
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID),
    FOREIGN KEY (ReferringDoctorID) REFERENCES Doctors(DoctorID)
);

-- Table for Surgeries
CREATE TABLE Surgeries (
    SurgeryID INT PRIMARY KEY,
    DoctorID INT,
    PatientID INT,
    SurgeryType VARCHAR(255),
    SurgeryDate DATE,
    FOREIGN KEY (DoctorID) REFERENCES Doctors(DoctorID),
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID)
);

-- Table for LaboratoryTests
CREATE TABLE LaboratoryTests (
    TestID INT PRIMARY KEY,
    DoctorID INT,
    LabTechnicianID INT,
    PatientID INT,
    TestType VARCHAR(255),
    TestDate DATE,
    TestResult VARCHAR(255),
    FOREIGN KEY (DoctorID) REFERENCES Doctors(DoctorID),
    FOREIGN KEY (LabTechnicianID) REFERENCES LabTechnicians(TechnicianID),
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID)
);

-- Table for Diagnoses
CREATE TABLE Diagnoses (
    DiagnosisID INT PRIMARY KEY,
    PatientID INT,
    DoctorID INT,
    DiagnosisDate DATE,
    DiagnosisDescription TEXT,
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID),
    FOREIGN KEY (DoctorID) REFERENCES Doctors(DoctorID)
);

-- Table for Appointments
CREATE TABLE Appointments (
    AppointmentID INT PRIMARY KEY,
    PatientID INT,
    DoctorID INT,
    AppointmentDateTime DATETIME,
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID),
    FOREIGN KEY (DoctorID) REFERENCES Doctors(DoctorID)
);


-- Table for Payments
CREATE TABLE Payments (
    PaymentID INT PRIMARY KEY,
    PatientID INT,
    PaymentDate DATE,
    Amount DECIMAL(10,2),
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID)
);

-- Table for Prescriptions
CREATE TABLE Prescriptions (
    PrescriptionID INT PRIMARY KEY,
    PatientID INT,
    DoctorID INT,
    DrugID INT,
    DatePrescribed DATE,
    FOREIGN KEY (PatientID) REFERENCES Patients(PatientID),
    FOREIGN KEY (DoctorID) REFERENCES Doctors(DoctorID),
    FOREIGN KEY (DrugID) REFERENCES Drugs(DrugID)
);

-- Table for LabTechnicians
CREATE TABLE LabTechnicians (
    TechnicianID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Gender VARCHAR(10),
    Salary DECIMAL(10,2),
    ExperienceYears INT
);

-- Table for Drugs
CREATE TABLE Drugs (
    DrugID INT PRIMARY KEY,
    DrugName VARCHAR(255),
    DosageStrength VARCHAR(255),
    DrugType VARCHAR(255),
    ExpiryDate DATE,
    ManufacturingDate DATE
);


CREATE TABLE Employees (
    EmpID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Gender VARCHAR(10),
    Salary DECIMAL(10,2),
    Position VARCHAR(255),
    AdminID INT,
    FOREIGN KEY (AdminID) REFERENCES Administrators(AdminID)
);

CREATE TABLE MedicalStaff (
    MedicalStaffID INT PRIMARY KEY,
    FirstName VARCHAR(50),
    LastName VARCHAR(50),
    Age INT,
    Sex VARCHAR(10),
    Salary DECIMAL(10,2),
    Position VARCHAR(255),
    ExperienceYears INT
);

CREATE TABLE Dosages (
    DosageID INT PRIMARY KEY,
    DosageName VARCHAR(255),
    Description TEXT
);
-- Table for Relation doctor_treats_patient
CREATE TABLE doctor_treats_patient (
    doctorID INT,
    patientID INT,
    PRIMARY KEY (doctorID, patientID),
    FOREIGN KEY (doctorID) REFERENCES Doctors(DoctorID),
    FOREIGN KEY (patientID) REFERENCES Patients(PatientID)
);

-- Table for Relation doctor_performes_surgery
CREATE TABLE doctor_performes_surgery (
    doctorID INT,
    surgeryID INT,
    PRIMARY KEY (doctorID, surgeryID),
    FOREIGN KEY (doctorID) REFERENCES Doctors(DoctorID),
    FOREIGN KEY (surgeryID) REFERENCES Surgeries(SurgeryID)
);
-- Table for Relation drug_dose
CREATE TABLE DrugDosage (
    DrugID INT,
    DosageID INT,
    PRIMARY KEY (DrugID, DosageID),
    FOREIGN KEY (DrugID) REFERENCES Drugs(DrugID),
    FOREIGN KEY (DosageID) REFERENCES Dosages(DosageID)
);
-- Table for Relation nurse_cares_patient
CREATE TABLE nurse_cares_patient (
    nurseID INT,
    patientID INT,
    PRIMARY KEY (nurseID, patientID),
    FOREIGN KEY (nurseID) REFERENCES Nurses(NurseID),
    FOREIGN KEY (patientID) REFERENCES Patients(PatientID)
);

-- Table for Relation nurse_assigned_room
CREATE TABLE nurse_assigned_room (
    nurseID INT,
    roomNumber INT,
    PRIMARY KEY (nurseID, roomNumber),
    FOREIGN KEY (nurseID) REFERENCES Nurses(NurseID),
    FOREIGN KEY (roomNumber) REFERENCES Rooms(RoomNumber)
);

-- Table for multivalued attribute Address
CREATE TABLE Address (
      entityID INT PRIMARY KEY,
      StreetAddress VARCHAR(255),
      City VARCHAR(100),
      State VARCHAR(100),
      PostalCode VARCHAR(20),
      Country VARCHAR(100)

);

-- Table for multivalued attribute emergency_contact
CREATE TABLE emergency_contact (
    patientID INT,
    emerg_contact_name VARCHAR(255),
    emerg_contact_phone int,
    PRIMARY KEY (patientID),
    FOREIGN KEY (patientID) REFERENCES Patients(PatientID)
);


--indexes and views
-- View for Patients table
CREATE VIEW Patients_View AS
SELECT PatientID, FirstName, LastName, Age, Gender, Weight, Height, DrID
FROM Patients;

-- View for Doctors table
CREATE VIEW Doctors_View AS
SELECT DoctorID, FirstName, LastName, Age, Gender, Specialization, ExperienceYears
FROM Doctors;

-- View for Nurses table
CREATE VIEW Nurses_View AS
SELECT NurseID, FirstName, LastName, Age, Gender, Salary, DepartmentID
FROM Nurses;

-- View for Rooms table
CREATE VIEW Rooms_View AS
SELECT RoomNumber, RoomType, NurseID, Status
FROM Rooms;

-- View for Administrators table
CREATE VIEW Administrators_View AS
SELECT AdminID, FirstName, LastName, Age, Gender, DepartmentID, Position
FROM Administrators;

-- View for Departments table
CREATE VIEW Departments_View AS
SELECT DepartmentID, DepartmentName, Description
FROM Departments;

-- View for Referrals table
CREATE VIEW Referrals_View AS
SELECT ReferralID, PatientID, ReferringDoctorID, ReferralHospital, ReferredHospital, ReferralReason, ReferralDate
FROM Referrals;

-- View for Surgeries table
CREATE VIEW Surgeries_View AS
SELECT SurgeryID, DoctorID, PatientID, SurgeryType, SurgeryDate
FROM Surgeries;

-- View for LaboratoryTests table
CREATE VIEW LaboratoryTests_View AS
SELECT TestID, DoctorID, LabTechnicianID, PatientID, TestType, TestDate, TestResult
FROM LaboratoryTests;

-- View for Diagnoses table
CREATE VIEW Diagnoses_View AS
SELECT DiagnosisID, PatientID, DoctorID, DiagnosisDate, DiagnosisDescription
FROM Diagnoses;

-- View for Appointments table
CREATE VIEW Appointments_View AS
SELECT AppointmentID, PatientID, DoctorID, AppointmentDateTime, AppointmentType
FROM Appointments;

-- View for Payments table
CREATE VIEW Payments_View AS
SELECT PaymentID, PatientID, PaymentDate, Amount
FROM Payments;

-- View for Prescriptions table
CREATE VIEW Prescriptions_View AS
SELECT PrescriptionID, PatientID, DoctorID, DrugID, DatePrescribed
FROM Prescriptions;

-- View for LabTechnicians table
CREATE VIEW LabTechnicians_View AS
SELECT TechnicianID, FirstName, LastName, Age, Gender, Salary, ExperienceYears
FROM LabTechnicians;

-- View for Drugs table
CREATE VIEW Drugs_View AS
SELECT DrugID, DrugName, DosageStrength, DrugType, ExpiryDate, ManufacturingDate
FROM Drugs;

-- View for DoctorPatientRelation table
CREATE VIEW DoctorPatientRelation_View AS
SELECT DoctorID, PatientID
FROM DoctorPatientRelation;

-- View for NursePatientRelation table
CREATE VIEW NursePatientRelation_View AS
SELECT NurseID, PatientID
FROM NursePatientRelation;

-- View for NurseRoomRelation table
CREATE VIEW NurseRoomRelation_View AS
SELECT NurseID, RoomNumber
FROM NurseRoomRelation;


-- //populating them with doctor_treats_patient

-- Populate Patients table
-- Populate Patients table
INSERT INTO Patients (PatientID, FirstName, LastName, Age, Gender, Weight, Height, DrID)
VALUES
(1, 'Meron', 'Tadesse', 30, 'Female', 60.5, 165, 1),
(2, 'Yohannes', 'Gebre', 25, 'Male', 70.8, 172, 2),
(3, 'Hana', 'Tekle', 40, 'Female', 65.2, 160, 3),
(4, 'Daniel', 'Berhanu', 35, 'Male', 75.3, 175, 1),
(5, 'Selam', 'Alem', 28, 'Female', 55.9, 168, 2),
(6, 'Amanuel', 'Mulugeta', 33, 'Male', 80.7, 180, 3),
(7, 'Elsa', 'Fikru', 45, 'Female', 70.1, 170, 1),
(8, 'Eyob', 'Wondimu', 29, 'Male', 78.4, 177, 2),
(9, 'Rahel', 'Mengistu', 38, 'Female', 62.6, 163, 3),
(10, 'Michael', 'Dawit', 27, 'Male', 85.2, 185, 1);

-- Populate Doctors table
INSERT INTO Doctors (DoctorID, FirstName, LastName, Age, Sex, Specialization, Experience, Salary, DepId)
VALUES
(1, 'Fitsum', 'Mengistu', 35, 'Male', 'Cardiology', 10, 15000.00, 1),
(2, 'Mahlet', 'Assefa', 40, 'Female', 'Orthopedics', 15, 18000.00, 2),
(3, 'Biniam', 'Girma', 38, 'Male', 'Pediatrics', 12, 16000.00, 3),
(4, 'Aster', 'Solomon', 42, 'Female', 'Oncology', 18, 17000.00, 1),
(5, 'Girma', 'Tekle', 36, 'Male', 'Neurology', 11, 15500.00, 2),
(6, 'Hiwot', 'Haile', 39, 'Female', 'Dermatology', 9, 16500.00, 3),
(7, 'Tilahun', 'Alemu', 41, 'Male', 'Pulmonology', 17, 17500.00, 1),
(8, 'Eden', 'Yohannes', 37, 'Female', 'Endocrinology', 13, 16500.00, 2),
(9, 'Fasil', 'Tadesse', 43, 'Male', 'Cardiothoracic Surgery', 16, 17000.00, 3),
(10, 'Helina', 'Tesfaye', 32, 'Female', 'Anesthesiology', 10, 16000.00, 1);

-- Populate Nurses table
INSERT INTO Nurses (NurseID, FirstName, LastName, Age, Sex, Salary)
VALUES
(1, 'Abiyot', 'Asrat', 28, 'Male', 9000.00),
(2, 'Tigist', 'Girma', 33, 'Female', 9500.00),
(3, 'Yonas', 'Tesfaye', 30, 'Male', 9200.00),
(4, 'Mekdes', 'Alemayehu', 35, 'Female', 9300.00),
(5, 'Daniel', 'Getachew', 27, 'Male', 8900.00),
(6, 'Eyerusalem', 'Mekonnen', 29, 'Female', 9100.00),
(7, 'Getachew', 'Abate', 31, 'Male', 9050.00),
(8, 'Betelhem', 'Assefa', 34, 'Female', 9400.00),
(9, 'Biruk', 'Tadesse', 26, 'Male', 9250.00),
(10, 'Senait', 'Teshome', 32, 'Female', 9600.00);

-- Populate Rooms table
INSERT INTO Rooms (RoomNumber, RoomType, NurseID, Status)
VALUES
(101, 'General', 1, 'Occupied'),
(102, 'Pediatric', 2, 'Available'),
(103, 'Surgical', 3, 'Under Maintenance'),
(104, 'Maternity', 4, 'Occupied'),
(105, 'Orthopedic', 5, 'Available'),
(106, 'ICU', 6, 'Occupied'),
(107, 'Emergency', 7, 'Available'),
(108, 'Labor', 8, 'Occupied'),
(109, 'Oncology', 9, 'Available'),
(110, 'Neurology', 10, 'Under Maintenance');

-- Populate Administrators table
INSERT INTO Administrators (AdminID, FirstName, LastName, Sex, Age, DepartmentID, Position)
VALUES
(1, 'Fatuma', 'Ahmed', 'Female', 45, 1, 'Head of Cardiology'),
(2, 'Mohammed', 'Ali', 'Male', 50, 2, 'Head of Orthopedics'),
(3, 'Amina', 'Omar', 'Female', 48, 3, 'Head of Pediatrics');

-- Populate Employees table
INSERT INTO Employees (EmployeeID, FirstName, LastName, Age, Sex, Position, Salary)
VALUES
(1, 'Fisseha', 'Gebre', 40, 'Male', 'Accountant', 12000.00),
(2, 'Samira', 'Mohammed', 35, 'Female', 'Human Resources Manager', 13000.00),
(3, 'Mulugeta', 'Tesfaye', 38, 'Male', 'IT Specialist', 12500.00),
(4, 'Zahra', 'Ali', 32, 'Female', 'Marketing Manager', 13500.00),
(5, 'Tekle', 'Woldemariam', 37, 'Male', 'Administrative Assistant', 11000.00),
(6, 'Hawariat', 'Yusuf', 33, 'Female', 'Customer Service Representative', 11500.00),
(7, 'Habtamu', 'Girma', 36, 'Male', 'Maintenance Technician', 10500.00),
(8, 'Amina', 'Seid', 31, 'Female', 'Legal Counsel', 14000.00),
(9, 'Fikru', 'Alemu', 34, 'Male', 'Quality Assurance Specialist', 12500.00),
(10, 'Asiya', 'Tekle', 30, 'Female', 'Public Relations Officer', 13000.00);

-- Populate Departments table
INSERT INTO Departments (DepartmentID, DepartmentName, Description, AdminID)
VALUES
(1, 'Cardiology', 'Specializing in heart diseases.', 1),
(2, 'Orthopedics', 'Specializing in musculoskeletal disorders.', 2),
(3, 'Pediatrics', 'Specializing in medical care for children.', 3);

-- Populate Referrals table
INSERT INTO Referrals (ReferralID, PatientID, ReferringDoctorID, ReferralHospital, ReferredHospital, ReferralReason, ReferralDate)
VALUES
(1, 1, 2, ' FELEGHIWIOT REFERAL HOSPITAL', ' KIDUS  PAULOS SPECAILAIZED  HOSPITAL', 'Specialist consultation', '2024-04-01'),
(2, 3, 4, 'FELEGHIWIOT REFERAL HOSPITAL', ' TIKUR ANBESA SPECAILAIZED HOSPITAL', 'Further tests required', '2024-04-03'),
(3, 5, 6, ' FELEGHIWIOT REFERAL HOSPITAL', ' TIKUR ANBESA SPECAILAIZED HOSPITAL', 'Surgical procedure', '2024-04-05'),
(4, 7, 8, ' FELEGHIWIOT REFERAL HOSPITAL', 'TIKUR ANBESA SPECAILAIZED HOSPITAL', 'Second opinion', '2024-04-07'),
(5, 9, 10, ' FELEGHIWIOT REFERAL HOSPITAL', 'AMNUEL MENTAL SPECAILAIZED  HOSPITAL', 'Advanced treatment', '2024-04-09');
